﻿using System;
using System.Collections.Generic;

namespace IsBeautifulString
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string s1 = "bbbaacdafe";//true
            string s2 = "aabbb";//false
            string s3 = "bbc";//false
            string s4 = "bbbaa";//false
            string s5 = "abcdefghijklmnopqrstuvwxyzz";//false
            string s6 = "abcdefghijklmnopqrstuvwxyz";//true
            string s7 = "abcdefghijklmnopqrstuvwxyzqwertuiopasdfghjklxcvbnm";//true
            string s8 = "fyudhrygiuhdfeis";//false
            string s9 = "zaa";//false
            string s10 = "zyy";//false



            System.Console.WriteLine(isBeautifulString(s1));//true
            System.Console.WriteLine(isBeautifulString(s2));//false
            System.Console.WriteLine(isBeautifulString(s3));//false
            System.Console.WriteLine(isBeautifulString(s4));//false
            System.Console.WriteLine(isBeautifulString(s5));//false
            System.Console.WriteLine(isBeautifulString(s6));//true
            System.Console.WriteLine(isBeautifulString(s7));//true
            System.Console.WriteLine(isBeautifulString(s8));//false
            System.Console.WriteLine(isBeautifulString(s9));//false
            System.Console.WriteLine(isBeautifulString(s10));//false

        }

        static bool isBeautifulString(string inputString) {
            List<char> c = new List<char>(inputString.ToCharArray());
            int count = c.FindAll(e => e == (char)97).Count;
            for (int i = 98; i < 123; i++)
            {
                if( c.FindAll(e => e == (char)97).Count == 0 && i == 98){
                    return false;
                } else{
                    c.RemoveAll(e => e == (char)97);
                }

                int c1 = c.FindAll(e => e == (char)i).Count;
                if((c1 > count)){
                    return false;
                }else if(count == 0 && c.Count == 0){
                    break;

                }else{
                    count = c1;
                    c.RemoveAll(e => e == (char)i);
                }
            }
            return true;
            
        }

    }
}
